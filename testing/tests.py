import unittest
from flask_testing import TestCase
from flask import url_for
from init import app, db, Group, Student


class BasicTests(TestCase):

    def create_app(self):
        app.config.from_object('app.test_setting')
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_index(self):
        response = self.client.get(url_for('index'))
        self.assertEqual(response.status_code, 200)

    def test_groups(self):
        response = self.client.get(url_for('groups'))
        self.assert200(response)
        return response

    def test_students(self):
        response = self.client.get(url_for('students'))
        self.assert200(response)
        return response

    def add_group(self):
        group = Group(name="unittest")
        db.session.add(group)
        db.session.commit()
        assert group in db.session

    def add_student(self):
        student = Student(name="Unittest", surname="Unittest", date_of_birth="2017-10-03", group_id=1)
        db.session.add(student)
        db.session.commit()
        assert student in db.session

    def test_edit_group_form(self):
        self.add_group()
        response = self.client.get(url_for('edit_group', group_id=1))
        self.assertEqual(response.status_code, 200)

    def test_edit_group(self):
        self.add_group()
        response = self.client.post(url_for('edit_group', group_id=1), data={'name': 'new_name'})
        self.assertEqual(response.status_code, 302)
        new_response = self.client.get(url_for('groups'))
        self.assert200(new_response)
        self.assertIn('new_name', str(new_response.data))

    def test_delete_group(self):
        self.add_group()
        response = self.client.get(url_for('delete_group', group_id=1))
        self.assertEqual(response.status_code, 302)

    def test_edit_student_form(self):
        self.add_group()
        self.add_student()
        response = self.client.get(url_for('edit_student', student_id=1))
        self.assertEqual(response.status_code, 200)

    def test_edit_student(self):
        self.add_group()
        self.add_student()
        response = self.client.post(url_for('edit_student', student_id=1), data={
            "name": "Edit_Unittest",
            "surname": "Edit_Unittest",
            "date_of_birth": "2017-10-03",
            "group_id": 1
        })
        self.assertEqual(response.status_code, 302)
        new_response = self.client.get(url_for('students'))
        self.assert200(new_response)
        self.assertIn('Edit_Unittest', str(new_response.data))

    def test_delete_student(self):
        self.add_group()
        self.add_student()
        response = self.client.get(url_for('delete_student', student_id=1))
        self.assertEqual(response.status_code, 302)

    def test_find_user(self):
        group = Group(name="unittest")
        db.session.add(group)
        student1 = Student(name="Unittest", surname="Unittest", date_of_birth="2017-10-03", group_id=1)
        student2 = Student(name="Unittest", surname="Unittest", date_of_birth="2017-08-03", group_id=1)
        db.session.add(group)
        db.session.add(student1)
        db.session.add(student2)
        db.session.commit()
        assert group in db.session
        assert student1 in db.session
        assert student2 in db.session
        response = self.client.get(url_for('find_student', group_id=group.id))
        self.assertIn('2017-08-03', str(response.data))


if __name__ == '__main__':
    unittest.main()
