from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('app.setting')
db = SQLAlchemy()
db.init_app(app=app)

from app.control import *
