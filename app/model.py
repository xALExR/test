from init import db


class Student(db.Model):
    __table_name__ = 'student'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    surname = db.Column(db.VARCHAR(255))
    date_of_birth = db.Column(db.Date)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id'))

    def __init__(self, name, surname, date_of_birth, group_id):
        self.name = name
        self.surname = surname
        self.date_of_birth = date_of_birth
        self.group_id = group_id

    def update_student(self, name, surname, date_of_birth):
        self.name = name
        self.surname = surname
        self.date_of_birth = date_of_birth
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class Group(db.Model):
    __table_name__ = 'group'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(255))
    student = db.relationship("Student", cascade="all, delete-orphan")

    def __init__(self, name):
        self.name = name

    def update_group(self, name):
        self.name = name
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def find_oldest_student(self, group_id):
        records = Student.query.join(Group).filter(Student.group_id == group_id).order_by(Student.date_of_birth).first()
        return records
