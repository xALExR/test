from wtforms import Form, validators, StringField, DateField


class StudentForm(Form):
    name = StringField('Name',  validators=[validators.Length(min=4, max=255)])
    surname = StringField('Surname', validators=[validators.Length(min=4, max=255)])
    date_of_birth = DateField('Date of birth', format='%Y-%m-%d')


class GroupForm(Form):
    name = StringField('Name', validators=[validators.Length(min=4, max=255)])