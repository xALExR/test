from run import app
from werkzeug.contrib.cache import SimpleCache
from flask import render_template, request, abort, redirect, url_for
from .model import Student, Group
from .forms import GroupForm, StudentForm
from functools import wraps

CACHE_TIMEOUT = 300
cache = SimpleCache()


class cached(object):
    def __init__(self, timeout=None):
        self.timeout = timeout or CACHE_TIMEOUT

    def __call__(self, f):
        @wraps(f)
        def decorator(*args, **kwargs):
            response = cache.get(request.path)
            if response is None:
                response = f(*args, **kwargs)
                cache.set(request.path, response, self.timeout)
            return response
        return decorator


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/student', methods=["GET", "POST"])
@cached(10)
def students():
    students = Student.query.order_by(Student.id).all()
    return render_template('students.html', students=students)


@app.route('/group', methods=["GET", "POST"])
def groups():
    groups = Group.query.order_by(Group.id).all()
    return render_template('group.html', groups=groups)


@app.route("/student/<int:student_id>", methods=["GET"])
def edit_student(student_id):
    student_form = StudentForm(request.form)
    try:
        student = Student.query.get(student_id)
    except Exception:
        return abort(404)
    if request.method == "GET":
        return render_template('edit_student.html', student=student, form=student_form)
    else:
        if student_form.validate():
            student.update_student(
                name=student_form.name.data,
                surname=student_form.surname.data,
                date_of_birth=student_form.date_of_birth.data)
            return redirect(url_for('students'))


@app.route("/group/<int:group_id>", methods=["GET", "POST"])
def edit_group_form(group_id):
    group_form = GroupForm(request.form)
    try:
        group = Group.query.get(group_id)
    except Exception:
        return abort(404)
    if request.method == "GET":
        return render_template('edit_group.html', group=group, form=group_form)
    else:
        if group_form.validate():
            group.update_group(name=group_form.name.data)
            return redirect(url_for('groups'))


@app.route('/student/delete/<int:student_id>', methods=["GET", "DELETE"])
def delete_student(student_id):
    try:
        student = Student.query.get(student_id)
        student.delete()
    except Exception:
        return abort(404)
    return redirect(url_for('students'))


@app.route('/group/delete/<int:group_id>', methods=["GET", "DELETE"])
def delete_group(group_id):
    try:
        group = Group.query.get(group_id)
        group.delete()
    except Exception:
        return abort(404)
    return redirect(url_for('groups'))


@app.route('/find_student/<int:group_id>', methods=["GET"])
def find_student(group_id):
    group = Group.query.get(group_id)
    oldest_student = group.find_oldest_student(group_id)
    return render_template('show_student.html', student=oldest_student)
