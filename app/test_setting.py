DB_TYPE = "postgresql"
DB_NAME = "xalex"
USER_NAME = "xalex"
USER_PASS = "root"
HOST_NAME = "localhost"
SQLALCHEMY_DATABASE_URI = '{db_type}+psycopg2://{user_name}:{user_pass}@{host_name}/{database}'.format(
    db_type=DB_TYPE, user_name=USER_NAME, user_pass=USER_PASS, host_name=HOST_NAME, database=DB_NAME)
TESTING = True
