import os

SECRET_KEY = "FDFTYtrf2y3tugey8236efdw7yb32ydqwqdwe"
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

#  configuration for postgresql
DB_TYPE = "postgresql"
DB_NAME = "postgres"
USER_NAME = "xalex"
USER_PASS = "root"
HOST_NAME = "localhost"
SQLALCHEMY_DATABASE_URI = '{db_type}+psycopg2://{user_name}:{user_pass}@{host_name}/{database}'.format(
    db_type=DB_TYPE, user_name=USER_NAME, user_pass=USER_PASS, host_name=HOST_NAME, database=DB_NAME)
# SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = True

